 {
 	"CallerReference": "NAME-DATE",
 	"Aliases": {
        "Quantity": 1,
        "Items": ["NAME.groupclique.com"]
    },
 	"DefaultRootObject": "index.html",
 	"Origins": {
 		"Quantity": 2,
 		"Items": [{
 				"Id": "NAME-static",
 				"DomainName": "S3_ORIGIN_REPLACE_ME",
 				"S3OriginConfig": {
 					"OriginAccessIdentity": ""
 				}
 			},
 			{
 				"Id": "NAME-dynamic",
 				"DomainName": "EC2_ORIGIN_REPLACE_ME",
 				"CustomOriginConfig": {
 					"HTTPPort": 80,
 					"HTTPSPort": 443,
 					"OriginProtocolPolicy": "https-only",
 					"OriginSslProtocols": {
 						"Quantity": 1,
 						"Items": ["TLSv1.2"]
 					},
 					"OriginReadTimeout": 30,
 					"OriginKeepaliveTimeout": 5
 				}

 			}
 		]
 	},
 	"DefaultCacheBehavior": {
 		"TargetOriginId": "NAME-static",
 		"ForwardedValues": {
 			"QueryString": true,
 			"Cookies": {
 				"Forward": "all"
 			},
 			"Headers": {
 				"Quantity": 3,
 				"Items": ["Access-Control-Request-Headers", "Access-Control-Request-Method", "Origin"]
 			}
 		},
 		"TrustedSigners": {
 			"Enabled": false,
 			"Quantity": 0
 		},
 		"ViewerProtocolPolicy": "redirect-to-https",
 		"MinTTL": 0,
 		"AllowedMethods": {
 			"Quantity": 7,
 			"Items": ["GET", "HEAD", "POST", "PUT", "PATCH", "OPTIONS", "DELETE"],
 			"CachedMethods": {
 				"Quantity": 2,
 				"Items": ["GET", "HEAD"]
 			}
 		},
 		"SmoothStreaming": false,
 		"DefaultTTL": 86400,
 		"MaxTTL": 31536000,
 		"Compress": true
 	},
 	"CacheBehaviors": {
 		"Quantity": 1,
 		"Items": [{
 			"PathPattern": "api/*",
 			"TargetOriginId": "NAME-dynamic",
 			"ForwardedValues": {
 				"QueryString": true,
 				"Cookies": {
 					"Forward": "all"
 				},
 				"Headers": {
 					"Quantity": 1,
 					"Items": ["Origin"]
 				}
 			},
 			"TrustedSigners": {
 				"Enabled": false,
 				"Quantity": 0
 			},
 			"ViewerProtocolPolicy": "https-only",
 			"MinTTL": 0,
 			"AllowedMethods": {
 				"Quantity": 7,
 				"Items": ["GET", "HEAD", "POST", "PUT", "PATCH", "OPTIONS", "DELETE"],
 				"CachedMethods": {
 					"Quantity": 2,
 					"Items": ["GET", "HEAD"]
 				}
 			},
 			"SmoothStreaming": false,
 			"DefaultTTL": 0,
 			"MaxTTL": 0,
 			"Compress": true
 		}]
 	},
 	"CustomErrorResponses": {
 		"Quantity": 2,
 		"Items": [{
 				"ErrorCode": 400,
 				"ResponsePagePath": "/index.html",
 				"ResponseCode": "200",
 				"ErrorCachingMinTTL": 300
 			},
 			{
 				"ErrorCode": 403,
 				"ResponsePagePath": "/index.html",
 				"ResponseCode": "200",
 				"ErrorCachingMinTTL": 300
 			}
 		]
 	},
 	"Comment": "NAME-DATE",
 	"Logging": {
 		"Enabled": LOG_ENABLED,
 		"IncludeCookies": true,
 		"Bucket": "LOG_BUCKET",
 		"Prefix": "LOG_PREFIX"
 	},
 	"PriceClass": "PriceClass_All",
 	"Enabled": true,
 	"ViewerCertificate": {
        "CloudFrontDefaultCertificate": false,
        "IAMCertificateId": "IAM_CERT_ID",
        "SSLSupportMethod": "sni-only",
        "MinimumProtocolVersion": "TLSv1.1_2016"
    }
 }