#!/bin/bash


function bucketExists() {
	# does the log bucket exist?
	bucket=$(aws s3api list-buckets --query "Buckets[?Name=='$1'].Name" --output text)
	if [ -n "$bucket" ]; then
    	return 1
	fi
	return 0
}

default_distribution_name="deleteme"
read -p "Distribution Name [$default_distribution_name]: " distribution_name 
distribution_name=${distribution_name:-$default_distribution_name}

echo -e "\nBuckets available for static origin: "
aws s3api list-buckets --query "Buckets[].Name" --output text > /tmp/availbuckets.txt
tr '\t' '\n' < /tmp/availbuckets.txt
echo ""

# static origin -- the S3 bucket that contains the angular code
default_static_origin="$distribution_name.groupclique.com.s3.amazonaws.com"
read -p "Static Origin [$default_static_origin]: " static_origin
static_origin=${static_origin:-$default_static_origin}

# dynamic origin -- either an EC2 instance or an ELB that is the API endpoint
echo -e "\nEC2 instances & Application ELBs available for dynamic origin: "
aws ec2 describe-instances --query "Reservations[].Instances[].{DNS:PublicDnsName}" --output text > /tmp/avail.txt
tr '\t' '\n' < /tmp/avail.txt
echo ""
aws elbv2 describe-load-balancers --query "LoadBalancers[].DNSName" --output text > /tmp/avail.txt
tr '\t' '\n' < /tmp/avail.txt
echo ""

default_dynamic_origin="$distribution_name.us-west-2.elb.amazonaws.com"
read -p "Dynamic Origin [$default_dynamic_origin]: " dynamic_origin
dynamic_origin=${dynamic_origin:-$default_dynamic_origin}

echo -e "\nBuilding CloudFront '$distribution_name' using: "
echo "Static Origin: "$static_origin
echo "Dynamic Origin: "$dynamic_origin
echo ""

read -p "Proceed? [y/N]" -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	echo -e "Stopping . . . \n"
    exit 1
fi


SERVER_CERTIFICATE_NAME="groupclique-wildcard-godaddy"

DATE=$(date +%Y%m%d%H%M%S)

# Logging
LOG_ENABLED=true
LOG_BUCKET="com.groupclique.logs"
LOG_PREFIX=$distribution_name

echo $(bucketExists ${LOG_BUCKET})

# create the sub-folder in the log bucket
etag=$(aws s3api put-object --bucket ${LOG_BUCKET} --key ${LOG_PREFIX}/ --query "ETag" --output text)

# get the SSL cert ID from IAM
iamCertId=$(aws iam list-server-certificates --query "ServerCertificateMetadataList[?ServerCertificateName=='${SERVER_CERTIFICATE_NAME}'].ServerCertificateId" --output text)

# Replace all of the property values in distconfig.tpl
cat ./distconfig.tpl |
    sed 's#S3_ORIGIN_REPLACE_ME#'${static_origin}'#g' |
    sed 's#EC2_ORIGIN_REPLACE_ME#'${dynamic_origin}'#g' |
    sed 's#COMMENT_REPLACE_ME#'${TAG}'#g' |
	sed 's#LOG_ENABLED#'${LOG_ENABLED}'#g' |
    sed 's#LOG_BUCKET#'${LOG_BUCKET}.s3.amazonaws.com'#g' |
    sed 's#LOG_PREFIX#'${LOG_PREFIX}'#g' |
    sed 's#NAME#'${distribution_name}'#g' |
    sed 's#DATE#'${DATE}'#g' |
    sed 's#IAM_CERT_ID#'${iamCertId}'#g' > /tmp/distconfig.json
    
response=$(aws cloudfront create-distribution --distribution-config file:///tmp/distconfig.json --output json)

outfile=/tmp/${distribution_name}-${DATE}.json
echo ${response} > ${outfile}
 
cfid=$(jq -r '.Distribution.Id' ${outfile})
etag=$(jq -r '.ETag' ${outfile})

echo -e "Distribution '$distribution_name' built with cfid: $cfid etag: $etag\n"
echo -e "Check the AWS Cloudfront console to know when the distribution is active!\n"
echo -e ""
echo -e "Be sure that the S3 static origin has the appropriate policy attached to it. e.g. \n"
cat bucketpolicy.tpl

echo "kthxbye"
