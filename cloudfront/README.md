#Precipitate

Quick AWS bash script to setup a Cloudfront distribution.

Values for the static and dynamic origins are not checked.  In this way the distribution can be built with origins that do not yet exist.

The static origin has the default behavior and expects that there will be an /index.hml 

The static S3 origin must be publically readable using a policy similar to 


	{
	    "Version": "2012-10-17",
	    "Statement": [
	        {
	            "Sid": "AddPerm",
	            "Effect": "Allow",
	            "Principal": "*",
	            "Action": "s3:GetObject",
	            "Resource": "arn:aws:s3:::testing.groupclique.com/*"
	        }
	    ]
	}



The dynamic origin behavior answers requests for api/*

##Note
S3 does some background DNS stuff that might make the CF distro redirect to the S3 bucket. This will *eventually* self correct itself or you *might* be able to circumvent this behavior by
enabling website hosting on the S3 bucket. See Also: [https://stackoverflow.com/questions/38735306/aws-cloudfront-redirecting-to-s3-bucket]()
