#!/bin/bash

function usage() {
    printf "\nUsage: $0 [dbidentifier]\n"
    printf "\tdbidentifier : The RDS instance identifier to upgrade\n\n"
    exit 1
}


if [[ $# -gt 1 ]]; then
        if [[ "$1" == "--help" || "$1" == "-h" ]]; then
                usage
        fi
fi

if [[ $# -eq 1 ]]; then
        dbid="$1"
else
        usage
fi

# Make certain this dbid exists and while we are at it, grab the instance class
instanceClass=$(aws rds describe-db-instances --db-instance-identifier ${dbid} --query DBInstances[].DBInstanceClass --output text)

rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Get the new DB identifier
read -p "New database identifier [$dbid]: " new_dbid
new_dbid=${new_dbid:-$dbid}

newDBInstanceIdentifier=""
if [[ $new_dbid != $dbid ]]
then	
	newDBInstanceIdentifier=" --new-db-instance-identifier $new_dbid"
fi

# Get the new instance size TODO: validate the size exists??
read -p "New RDS instance size [$instanceClass]: " newInstanceClass
newInstanceClass=${newInstanceClass:-$instanceClass}

dbInstanceClass=""
if [[ $newInstanceClass != $instanceClass ]]
then
	dbInstanceClass=" --db-instance-class $newInstanceClass"
fi

# Check that we actually have work to do
if [[ -z "$dbInstanceClass" && -z "$newDBInstanceIdentifier" ]]; then
	printf "\no changes?  Exiting.\n\n"
	exit 1
fi

# Last chance to bail out gracefully
read -p "Modify database $dbid with $newDBInstanceIdentifier $dbInstanceClass? [y/N] " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
	printf "Stopping . . . \n"
    exit 1
fi

now=$(date)
printf "\nStarting modification to $dbid $now\n\n"

arn=$(aws rds describe-db-instances --db-instance-identifier ${dbid} --query DBInstances[].DBInstanceArn --output text)
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

read -p "Create snapshot? [y/N] " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
	
	printf "\nCreating snapshot -- this could take a few minutes.\n\n"
	
	# Create a snapshot of the current database
	dateTime=`date "+%Y%m%d-%H%M%S"`
	snapshotID=$dbid-$dateTime
	foo=$(aws rds create-db-snapshot --db-snapshot-identifier $snapshotID --db-instance-identifier $dbid)
	rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
	
	snapshotStatus=$(aws rds describe-db-snapshots --db-snapshot-identifier $snapshotID --query DBSnapshots[].Status --output text)
	
	# Spinner!!
	sp="/-\|"
	sc=0
	
	# Wait for the snapshot to be complete
	slept=0
	while [  "$snapshotStatus" != "available" ]; do
		snapshotPercent=0
		snapshotPercent=$(aws rds describe-db-snapshots --db-snapshot-identifier $snapshotID --query DBSnapshots[].PercentProgress --output text)
		
	    sleep 0.1
	    ((slept++))
	    if [ "$slept" -eq 50 ]; then
		    slept=0
			snapshotStatus=$(aws rds describe-db-snapshots --db-snapshot-identifier $snapshotID --query DBSnapshots[].Status --output text) 
		fi
		
		# aws returns 100% with a status not "available".  pretty this up with a lie.
		if [[ "$snapShotStatus" != "available" && "$snapshotPercent" -eq 100 ]]; then
			snapshotPercent=99
		fi
		
		printf "\r ${sp:sc++:1}  $snapshotStatus snapshot $snapshotID ($snapshotPercent%%)       "
		((sc==${#sp})) && sc=0
	done
	
	now=$(date)
	printf "\nSnapshot complete! $now\n\n"

fi

modcmd=$(aws rds modify-db-instance --db-instance-identifier $dbid $newDBInstanceIdentifier $dbInstanceClass --apply-immediately)

rc=$?
if [[ $rc != 0 ]]; then
	printf "Something went wrong.  Most likely the instance class is invalid.\n"
	exit $rc
fi

printf "Hanging out until the the mod is complete . . .\n\n"
	
status="initializing"
slept=0

# aws doesn't take status out of "available" immediately.  set a flag to know that status has passed through "modifying"
hasBeenModified=0

# Spinner!!
sp="/-\|"
sc=0

while [[ "$status" != "available" || "$hasBeenModified" -eq 0 ]]; do
	sleep 0.1
	((slept++))
	if [ "$slept" -eq 50 ]; then
		slept=0
		status=$(aws rds describe-db-instances --db-instance-identifier $arn --query DBInstances[].DBInstanceStatus --output text)
		if [ "$status" = "modifying" ]; then
			hasBeenModified=1
		fi
	fi
	printf "\r ${sp:sc++:1} $arn status is $status         "
	((sc==${#sp})) && sc=0
done

now=$(date)
printf "\n\nDone! $now\n"

exit 0