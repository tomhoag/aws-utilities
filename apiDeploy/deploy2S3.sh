#!/bin/sh


BASH_FILE="HelloWorld.sh"
S3_BUCKET="com.groupclique.deploy-api"

ZIPFLAGS="-q"
# Delete zip from filesystem when done?
FS_DELETEZIP=1

# Delete zip from S3 when done?
S3_DELETEZIP=1

WD=$(pwd)

ts=$(date +%s)
zipname="archive$ts.zip"

# Pulled lib/python2.7/dist-packages from an ec2-instance with pip install run for all dependencies
lib1="lib/python2.7/dist-packages/"
lib64="lib64/python2.7/dist-packages/"

printf "zipping lambda_function.py\n"
zip "$ZIPFLAGS" $zipname lambda_function.py

printf "zipping $lib1\n"
cd "$WD/$lib1"
zip "$ZIPFLAGS" -r "$WD/$zipname" .

printf "zipping $lib64\n"
cd "$WD/$lib64"
zip "$ZIPFLAGS" -r "$WD/$zipname" .

cd "$WD"

printf "copying $zipname to s3://$S3_BUCKET\n"
aws s3 cp "$zipname" "s3://$S3_BUCKET"

base=$(basename $zipname)
url="https://s3-us-west-2.amazonaws.com/$S3_BUCKET/$base"
echo $url | pbcopy
printf "copied $url to pasteboard\n"

printf "updating lambda function with $base\n"
aws lambda update-function-code  --function-name DeployAPI_Lambda --s3-bucket "$S3_BUCKET" --s3-key "$base"

if [ "$FS_DELETEZIP" == 1 ]; then 
    printf "removing $zipname from filesystem\n"
    /bin/rm "$zipname"
fi

if [ "$S3_DELETEZIP" == 1 ]; then 
    printf "removing $zipname from S3\n"
    uri="s3://$S3_BUCKET/$base"
    aws s3 rm "$uri"
fi

printf "coping $BASH_FILE to s3://$S3_BUCKET\n"
aws s3 cp $BASH_FILE "s3://$S3_BUCKET"
