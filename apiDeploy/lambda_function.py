import boto3
import paramiko
import os

def lambda_handler(event, context):

    tags=event['instanceTags']
    branch=event['branch']

    print "Executing %s:%s using tag: %s and branch: %s" % (context.function_name, __name__, tags, branch)

    #Get IP addresses of EC2 instances
    client = boto3.client('ec2')
    instDict=client.describe_instances(
            Filters=[{'Name':'tag:Environment','Values':tags}]
        )

    hostList=[]
    for r in instDict['Reservations']:
        for inst in r['Instances']:
            hostList.append(inst['PublicIpAddress'])

    #Invoke worker function for each IP address
    client = boto3.client('lambda')
    for host in hostList:
        print "Deploying %s on %s" %(branch, host)
        deploy_code(host, branch)

    return{
        'message' : "Trigger function finished"
    }

def deploy_code(host, branch):

    s3_client = boto3.client('s3')
    #Download private key file from secure S3 bucket
    s3_client.download_file('com.groupclique.deploy-api','reeagc.pem', '/tmp/reeagc.pem')

    k = paramiko.RSAKey.from_private_key_file("/tmp/reeagc.pem")
    c = paramiko.SSHClient()
    c.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    print "Connecting to " + host
    c.connect( hostname = host, username = "ec2-user", pkey = k )
    print "Connected to " + host

    commands = [
        "aws s3 cp s3://com.groupclique.deploy-api/HelloWorld.sh /home/ec2-user/HelloWorld.sh",
        "chmod 700 /home/ec2-user/HelloWorld.sh ",
        "/home/ec2-user/HelloWorld.sh " + branch,
        "rm /home/ec2-user/HelloWorld.sh",
        ]
    for command in commands:
        print "Executing {}".format(command)
        stdin , stdout, stderr = c.exec_command(command)
        print stdout.read()
        print stderr.read()

    return
    {
        'message' : "Script execution completed. See Cloudwatch logs for complete output"
    }
