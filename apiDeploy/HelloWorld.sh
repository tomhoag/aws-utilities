#!/bin/bash

printf "Hello World, yo\n"

if [[ $# -eq 1 ]]; then
    branch="$1"
else
    printf "No branch on the command line. Exiting.\n\n"
    exit 1
fi

eval `ssh-agent -s`
keys=$(ssh-add -l)

if echo "$keys" | grep -q "/home/ec2-user/.ssh/gceng"; then
  echo "";
else
  printf "Missing ssh key -- adding . . .\n";
  aws s3 cp s3://com.groupclique.deploy-api/gceng /home/ec2-user/.ssh/gceng
  chmod 600 /home/ec2-user/.ssh/gceng
  ssh-add /home/ec2-user/.ssh/gceng
fi

cd /home/ec2-user/app/api

git checkout "$branch"

git pull origin "$branch"
