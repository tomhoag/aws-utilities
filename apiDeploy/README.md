# README #

An integration of bitbucket pipelines, AWS lambda functions and a bash file (stored in an AWS S3 bucket) to refresh the code running on an AWS ec2 instance.

https://aws.amazon.com/blogs/compute/scheduling-ssh-jobs-using-aws-lambda/

* lambda_function.py - The code that runs in the AWS lambda function
* HelloWorld.sh - The bash file that refreshes the code
* bitbucket-pipelines.yml - The bitbucket pipeline directive

## How do I get set up? ##

###1. Create AWS IAM role for AWS Lambda function###

Your lambda function will need a role that allows basic lambda execution and the ability to list your ec2 instances.  

Using IAM, create a new Policy called ec2Describe:

1.1 IAM &rarr; Policies &rarr; Create Policy

1.2 Using the JSON tab, copy and paste the following into the policy editor:

    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Stmt1447799371000",
                "Effect": "Allow",
                "Action": [
                    "ec2:Describe*"
                ],
                "Resource": [
                    "*"
                ]
            }
        ]
    }
    ```

1.3. Click 'Review Policy'

1.4. Provide a name (ec2Describe) and a description.

1.5. Click 'Create Policy'

Now create an IAM role and attach the policy just created (ec2Describe) and the AWS managed policy AWSLambdaExecute:

1.5 IAM &rarr; Roles &rarr; Create Role

1.6 Select 'AWS Service' and 'Lambda'

1.7 Click 'Next: Permissions'

1.8 Use the search box and select 'AWSLambdaExecute'

1.9 Change the 'Filter:' dropdown (next to the search box) to 'Customer managed'.  Find your ec2Describe policy and select it too.

1.10 Select 'Next: Review'

1.11 Give the role a name (lambdaDeploy) and a description.  Click 'Create Role'

###2. Create AWS Lambda function###

Create an AWS Lambda function that will be triggered by a bitbucket pipeline.  The function will ssh into specific ec2 instances and execute a simple bash script.

2.1 Lambda &rarr; Functions &rarr; Create function

2.2 Selct 'Author from scratch'

2.3 Give your function a name (deployer), Runtime: Python 2.7

2.4 For Role: select 'Choose an existing role'

2.5 For Existing role: select the role you just created (lambdaDeploy)

2.6 Click 'Create function'

Now we will write the code and package it up for use with your new Lambda function.

###3. Create the function code###

The Lambda execution environment requires that you provide all of your dependencies (except boto3).  Moreover, you need to provide both the lib and lib64 versions of the dependencies.

3.1. Create


* Create AWS Lambda function
* Create AWS S3 bucket
* HelloWorld.sh 
* 
* Dependencies

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
