# aws-utilities


Repo to hold useful(?) aws scripts

[cloudfront](cloudfront/README.md) -- Build a CloudFront distribution for the Web App and Web API

[rdsUpgrade](rdsUpgrade/?at=master) -- Scripted upgrade of an RDS instance -- change the name and instance size

[apiDeploy](apiDeploy/) -- Bitbucket pipeline to trigger Lambda function to run bash script on an EC2 instance

[ec2_RDS_StartStop](ec2_RDS_StartStop/) -- start/stop lambda functions used in conjunction with cloud watch to start up and stop EC2 and RDS instances on a cron schedule.
