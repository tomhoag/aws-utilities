# Simple boto3 scripts to start and stop ec2 and rds instances ##
 
* These scripts can be tied to [CloudWatch events](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/WhatIsCloudWatchEvents.html) using cron to start/stop instances when not needed.  i.e. dev/test environments that are using M-F 7a-7p

* Tag instances with key:turndownservice value:yes and key:wakeupservice value:yes.  (Change the value to 'no' to turn off the service w/o messing with the CloudWatch events)

* [Handy AWS Cron Guide](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html#CronExpressions)


* The lambda functions need roles assigned with the corresponding policies.  

* The lambda functions require an environment variable 'region' that correspondes to the region in which the resources (EC2, RDS) reside

* To test the lambda functions, create a test event :

```
{"turndownservice":"yes"}
```

* Set the timeout to 10s

* Use Python 3.6 for the runtime

* Handler: lambda_function.lambda_handler


 