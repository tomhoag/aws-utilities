import botocore
import boto3
import os

REGION = os.environ['region']

# handy cloudwatch cron guide
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html#CronExpressions

def lambda_handler(event, context):

    if len(event) != 1:
        print("Received event with too many tags {}".format(event))
        return
    
    tagname=next(iter(event))
    tagvalue = event[tagname]

    Filters = [
        {'Name': 'tag:{}'.format(tagname), 'Values': [tagvalue] },
        {'Name': 'instance-state-name', 'Values': ['running']}
    ]

    def get_tags_for_db(db):
        instance_arn = db['DBInstanceArn']
        instance_tags = rds.list_tags_for_resource(ResourceName=instance_arn)
        return instance_tags['TagList']
    
    rds = boto3.client('rds', region_name=REGION)
    dbs = rds.describe_db_instances()
    
    for db in dbs['DBInstances']:
        #try:
        #    print ("{}@{}:{} {}".format(
        #        db['MasterUsername'],
        #        db['Endpoint']['Address'],
        #        db['Endpoint']['Port'],
        #        db['DBInstanceStatus']))
        #except:
        #    continue
            
        db_tags = get_tags_for_db(db)
        for tag in db_tags:
            if str(tag['Key']) == tagname and str(tag['Value']) == tagvalue:
                if db['DBInstanceStatus'] == 'stopped':
                    rds.start_db_instance(DBInstanceIdentifier=db['DBInstanceIdentifier'])
                    print("started {}".format(db['DBInstanceIdentifier']))
