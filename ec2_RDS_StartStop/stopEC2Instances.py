import boto3
import os
import json

REGION = os.environ['region']

# handy cloudwatch cron guide
# https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html#CronExpressions

def lambda_handler(event, context):

    if len(event) != 1:
        return
    
    tagname=next(iter(event))
    tagvalue = event[tagname]

    ec2 = boto3.resource('ec2', region_name=REGION)

    Filters = [
        {'Name': 'tag:{}'.format(tagname), 'Values': [tagvalue] },
        {'Name': 'instance-state-name', 'Values': ['running']}
    ]
    
    instances = ec2.instances.filter(Filters=Filters)
    ids = []
    for i in instances:
        ids.append(i.id)

    # stop 'em (if ids is empty it will match *all* ids in the filter -- wtf?)
    if(len(ids) > 0):
        print("stopping {}".format(ids))
        ec2.instances.filter(InstanceIds=ids).stop()
